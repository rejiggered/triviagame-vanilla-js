var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync').create();
var include = require('gulp-include');
var uglify = require('gulp-uglify-es').default;

var input = './src/scss/**/*.scss';
var output = '.';


gulp.task('sass', function () {
	return gulp.src(input)
	.pipe(sass({ outputStyle: 'compressed' }))
	.on('error', sass.logError)
	.pipe(autoprefixer({ browsers: ['last 2 versions', '> 5%', 'Firefox ESR'] }))
	.pipe(gulp.dest(output))
	.pipe(browserSync.stream());
});


// Concat plugins
gulp.task('js', function() {
    gulp.src(['dev/js/app.js'])
        .pipe( include() )
    //    .pipe( uglify()	)
        .pipe( gulp.dest(".") );
});



// Watch files for change and set Browser Sync
gulp.task('watch', function() {
	// BrowserSync settings
	browserSync.init({
	//proxy: "hrkiosk.com",
	files: "styles.css",
	notify: false,
	browser: "google chrome",
	server: true
});





// Scss file watcher
gulp.watch(input, ['sass']);
gulp.watch('*.html').on('change', browserSync.reload);
gulp.watch(('dev/js/main.js')).on('change', browserSync.reload);
});



// Default task
gulp.task('default', ['sass', 'watch', 'js']);
